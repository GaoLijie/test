package domain;

import org.junit.Assert;
import org.junit.Test;

public class TestDog {
    private Dog dog = new Dog("Jhon", 10);

    @Test
    public void testRun() {
        int expected = 180;
        Assert.assertEquals(expected, dog.run(180));
    }
}

package domain;

import org.junit.Assert;
import org.junit.Test;

public class TestPerson {
    private Person person = new Person("Jhon", 30, 180);

    @Test
    public void testTalk() {
        String expected = "hello, my name is Jhon";
        Assert.assertEquals(expected, person.talk());
    }

    @Test
    public void testJump() {
        int expected = 180 + 20;
        Assert.assertEquals(expected, person.jump(20));
    }
}

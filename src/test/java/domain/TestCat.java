package domain;

import org.junit.Assert;
import org.junit.Test;

public class TestCat {
    private Cat cat = new Cat("Jhon");


    @Test
    public void testGetName() {
        String expected = "Jhon";
        Assert.assertEquals(expected, cat.getName());
    }
}
